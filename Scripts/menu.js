// Menu button animation

const menuBtn = document.querySelector('.menuBtn')
let menuOpen = false

menuBtn.addEventListener('click', () => {
    if (!menuOpen) {
        menuBtn.classList.add('open')
        menuOpen = true
    } else {
        menuBtn.classList.remove('open')
        menuOpen = false
    }
})

// Menu overlay

var t1 = new TimelineMax({paused: true})

t1.to(".menu", 2, {
    right: "0%",
    ease: Expo.easeInOut,
    delay: -2
})

t1.staggerFrom(".menu ul li", 2, {x: -200, opacity: 0, ease:Expo.easeInOut}, 0.3)

t1.reverse()
$(document).on("click", ".menuBtn, li", function() {
    t1.reversed(!t1.reversed())
})



// Menu blocks animation

// var t1 = new TimelineMax({
//     paused: true
// })

// t1.to('.block', 1.2, {
//     width: "25%",
//     ease: Power4.easeInOut
// })

// t1.staggerFrom('.menu ul li', 0.8, {
//     y: 40,
//     opacity: 0,
//     ease: Power2.easeOut
// }, 0.2)

// t1.reverse()
// $(document).on('click', '.menuBtn', function () {
//     t1.reversed(!t1.reversed())
// })